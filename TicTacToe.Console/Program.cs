﻿using System;
using TicTacToe.Domain;

namespace TicTacToe.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            TicTacToeGame game = new TicTacToeGame();

            var currentPlayer = game.Player_X;
            while (game.Board.GetWinner() == null)
            {
                ConsoleTicTacTowPrinter(game);
                var currentMove = GetPlayerInput(currentPlayer);
                try
                {
                    game.MakeMove(currentPlayer, currentMove.x, currentMove.y);
                    currentPlayer = currentPlayer == game.Player_X ? game.Player_O : game.Player_X;
                }
                catch (InvalidOperationException invalidOperationException)
                {
                    System.Console.WriteLine(invalidOperationException.Message);
                }
            }

            PrintWinner(game);
        }

        static void PrintWinner(TicTacToeGame game)
        {
            ConsoleTicTacTowPrinter(game);
            System.Console.WriteLine(new string('*', 50));
            System.Console.WriteLine("Winner: " + game.Board.GetWinner().Type);
            System.Console.WriteLine(new string('*', 50));
        }

        static (int x, int y) GetPlayerInput(Player player)
        {
            var moveValid = false;
            int x = -1;
            int y = -1;
            while (!moveValid)
            {
                System.Console.WriteLine($"Player-{player.Type} please choose your move (format): row,column");
                var input = System.Console.ReadLine().Split(',');
                moveValid = (input.Length == 2 
                    && int.TryParse(input[0], out x) 
                    && int.TryParse(input[1], out y)
                    && (x > 0 && x <= GameBoard.GRIDSIZE)
                    && (y > 0 && y <= GameBoard.GRIDSIZE));
                if (!moveValid)
                {
                    System.Console.WriteLine("Invalid move entered");
                }
            }
            return (x - 1, y - 1);
        }

        /// <summary>
        /// Quick and dirty utility method to print a game on console
        /// </summary>
        /// <param name="game">The game to print to console</param>
        static void ConsoleTicTacTowPrinter(TicTacToeGame game)
        {
            const int WIDTH = 10;

            System.Console.Clear();
            for (int x = 0; x < GameBoard.GRIDSIZE; x++)
            {
                System.Console.WriteLine(new string('-', (WIDTH + 2) * 3));
                for (int y = 0; y < GameBoard.GRIDSIZE; y++)
                {
                    var cell = game.Board.GetCell(x, y);
                    var cellMark = cell.Player == null ? " " : cell.Player.Type.ToString();
                    System.Console.Write(new string(' ', WIDTH / 2) + cellMark + new string(' ', WIDTH / 2) + "|");
                }
                System.Console.WriteLine();
            }
            System.Console.WriteLine(new string('-', (WIDTH + 2) * 3));
        }

    }
}

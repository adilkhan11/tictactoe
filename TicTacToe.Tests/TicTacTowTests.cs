using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TicTacToe.Domain;

namespace TicTacToe.Tests
{
    [TestClass]
    public class TicTacTowTests
    {
        [TestMethod]
        public void Should_Have_A_Game_Board_When_Created()
        {
            // Arrange
            var game = new TicTacToeGame();

            // Act
            var board = game.Board;

            // Assert
            Assert.IsNotNull(board);
        }

        [TestMethod]
        public void Should_Throw_Exception_When_Player_O_Makes_First_Move()
        {
            // Arrange
            var game = new TicTacToeGame();

            // Act, Assert
            Assert.ThrowsException<InvalidOperationException>(() => game.MakeMove(game.Player_O, 1, 2));
        }

        [TestMethod]
        public void Should_Return_Winning_Player_When_Player_Makes_Winning_Move()
        {
            // Arrange
            var game = new TicTacToeGame();

            // Act
            var player = game.Player_X;

            // Assert
            Assert.IsNull(game.MakeMove(player, 1, 1));
            Assert.IsNull(game.MakeMove(player, 1, 2));
            Assert.AreEqual(player, game.MakeMove(player, 1, 0));
        }

    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TicTacToe.Domain;

namespace TicTacToe.Tests
{
    [TestClass]
    public class GameBoardTests
    {
        [TestMethod]
        public void Should_Have_Two_Players_When_Created()
        {
            // Arrange
            var board = new GameBoard();

            // Act, Assert
            Assert.IsNotNull(board.Player_X);
            Assert.IsNotNull(board.Player_O);
        }

        [TestMethod]
        public void Should_Have_Correct_Player_Names_When_Created()
        {
            // Arrange
            var gameBoard = new GameBoard();

            // Act, Assert
            Assert.AreEqual(gameBoard.Player_X.Type, PlayerType.X);
            Assert.AreEqual(gameBoard.Player_O.Type, PlayerType.O);
        }

        [TestMethod]
        public void Should_Have_Grid_Size_of_3x3_When_Created()
        {
            // Arrange, Act
            var gameBoard = GameBoard.GRIDSIZE;

            // Assert
            Assert.AreEqual(gameBoard, 3);
        }

        [TestMethod]
        public void Should_Have_All_Cells_Initialized_When_Created()
        {
            // Arrange, Act
            var gameBoard = new GameBoard();

            // Assert
            for (int x = 0; x < GameBoard.GRIDSIZE; x++)
            {
                for (int y = 0; y < GameBoard.GRIDSIZE; y++)
                {
                    Assert.IsNotNull(gameBoard.GetCell(x, y));
                }
            }
        }


        [TestMethod]
        public void Should_Not_Allow_Player_Move_To_Already_Occupied_Cell()
        {
            // Arrange
            var gameBoard = new GameBoard();
            var gameBoardCell = gameBoard.GetCell(0, 0);
            gameBoardCell.Mark(gameBoard.Player_X);

            // Act
            var moved = gameBoardCell.Mark(gameBoard.Player_O);

            // Assert
            Assert.IsFalse(moved);
        }

        [TestMethod]
        public void Should_Allow_Player_To_Mark_Non_Occupied_Cell()
        {
            // Arrange
            var gameBoard = new GameBoard();

            // Act
            var moved = gameBoard.Player_X.Move(0, 0);

            // Assert
            Assert.IsTrue(moved);
        }

        [TestMethod]
        public void Should_Return_Winning_Player_When_Horizontal_Line_Marked_By_A_Player()
        {
            // Arrange
            var gameBoard = new GameBoard();

            // Act
            gameBoard.Player_X.Move(0, 0);
            gameBoard.Player_X.Move(0, 1);
            gameBoard.Player_X.Move(0, 2);

            var winner = gameBoard.GetWinner();

            // Assert
            Assert.AreEqual(gameBoard.Player_X, winner);
        }

        [TestMethod]
        public void Should_Return_Winning_Player_When_Vertical_Line_Marked_By_A_Player()
        {
            // Arrange
            var gameBoard = new GameBoard();

            // Act
            gameBoard.Player_O.Move(2, 2);
            gameBoard.Player_O.Move(0, 2);
            gameBoard.Player_O.Move(1, 2);

            var winner = gameBoard.GetWinner();

            // Assert
            Assert.AreEqual(gameBoard.Player_O, winner);
        }

        [TestMethod]
        public void Should_Return_Winning_Player_When_First_Diagonal_Line_Marked_By_A_Player()
        {
            // Arrange
            var gameBoard = new GameBoard();

            // Act
            gameBoard.Player_O.Move(0, 0);
            gameBoard.Player_O.Move(1, 1);
            gameBoard.Player_O.Move(2, 2);

            var winner = gameBoard.GetWinner();

            // Assert
            Assert.AreEqual(gameBoard.Player_O, winner);
        }

        [TestMethod]
        public void Should_Return_Winning_Player_When_Second_Diagonal_Line_Marked_By_A_Player()
        {
            // Arrange
            var gameBoard = new GameBoard();

            // Act
            gameBoard.Player_X.Move(2, 0);
            gameBoard.Player_X.Move(1, 1);
            gameBoard.Player_X.Move(0, 2);

            var winner = gameBoard.GetWinner();

            // Assert
            Assert.AreEqual(gameBoard.Player_X, winner);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using TicTacToe.Domain;

namespace TicTacToe.Tests
{
    [TestClass]
    public class GameBoardCellTests
    {
        [TestMethod]
        public void Should_Have_No_Player_Set_When_Created()
        {
            // Arrange
            var gameBoardCell = new GameBoardCell(0, 0);

            // Act, Assert
            Assert.IsNull(gameBoardCell.Player);
        }

    }
}


# TicTacTow Game
A simple implementation of the tic tak toe game in c#

## Game Strategy
- The TicTacTow game is *played* by two **players** on a 3x3 **board**. 
- Initially all **cells** on the **game board** are **vacent**. 
- Players make a *move* one after the other. 
- A *move* made by a **player** *marks* a **cell** as occupied
- A move can only be made on a cell that is not occupied. 
- Once a move is made by a player, if a straight line is found marked by 
a single player either horizontally, vertically or in a diagonal, then the game 
is won by that player
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe.Domain
{
    public class GameBoardCell
    {
        public GameBoardCell(int x, int y)
        {
            X = x;
            Y = y;
        }
        public Player Player { get; private set; }

        public bool Marked => Player != null;

        public int X { get; }
        public int Y { get; }

        public bool Mark(Player player)
        {
            if (player == null)
            {
                throw new ArgumentNullException(nameof(player));
            }

            if (Marked)
            {
                return false;
            }

            Player = player;
            return true;
        }

        public override string ToString()
        {
            return $"({X},{Y}) - Player{Player?.Type}";
        }
    }
}

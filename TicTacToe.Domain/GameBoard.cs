﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace TicTacToe.Domain
{
    public class GameBoard
    {
        public const int GRIDSIZE = 3;
        protected GameBoardCell[][] Cells;
        public Player Player_X { get; private set; }
        public Player Player_O { get; private set; }

        public bool AnyCellMarked => Cells.SelectMany(o => o).Any(o => o.Marked);

        public GameBoard()
        {
            InitializeBoard();

            Player_X = new Player(this, PlayerType.X);
            Player_O = new Player(this, PlayerType.O);
        }

        /// <summary>
        /// Initialize the game board with empty cells
        /// </summary>
        private void InitializeBoard()
        {
            Cells = new GameBoardCell[GRIDSIZE][];
            for (int i = 0; i < GRIDSIZE; i++)
            {
                Cells[i] = new GameBoardCell[GRIDSIZE];
                for (int j = 0; j < GRIDSIZE; j++)
                {
                    Cells[i][j] = new GameBoardCell(i, j);
                }
            }
        }


        public GameBoardCell GetCell(int x, int y)
        {
            if ((x < 0 || x >= GRIDSIZE) || (y < 0 || y >= GRIDSIZE))
            {
                throw new ArgumentOutOfRangeException("Invalid cell position entered");
            }

            return Cells[x][y];
        }

        public IList<GameBoardCell> CellsFlattened => Cells.SelectMany(o => o).ToList();

        /// <summary>
        /// This will calculate if we have a winner on the game board
        /// </summary>
        /// <returns>Returns the winning Player object in case of a winner else it will return null</returns>
        public Player GetWinner()
        {
            if (PlayerMatchHorizontal(Player_X.Type)
                || PlayerMatchVertical(Player_X.Type)
                || PlayerMatchDiagnols(Player_X.Type))
            {
                return Player_X;
            }

            if (PlayerMatchHorizontal(Player_O.Type) 
                || PlayerMatchVertical(Player_O.Type)
                || PlayerMatchDiagnols(Player_O.Type))
            {
                return Player_O;
            }

            return null;
        }

        /// <summary>
        /// This will check if the provided player type (X or O) has any horizontal line marked
        /// </summary>
        /// <param name="playerType">The player type to check any horizontal line marked</param>
        /// <returns>Returns true if a the player has any horizontal line marked</returns>
        private bool PlayerMatchHorizontal(PlayerType playerType)
        {
            return CellsFlattened.Where(o => o.Player?.Type == playerType).GroupBy(o => o.X).Where(o => o.Count() == GRIDSIZE).Count() > 0;
        }

        /// <summary>
        /// This will check if the provided player type (X or O) has any vertical line marked
        /// </summary>
        /// <param name="playerType">The player type to check any vertical line marked</param>
        /// <returns>Returns true if a the player has any vertical line marked</returns>
        private bool PlayerMatchVertical(PlayerType playerType)
        {
            return CellsFlattened.Where(o => o.Player?.Type == playerType).GroupBy(o => o.Y).Where(o => o.Count() == GRIDSIZE).Count() > 0;
        }

        /// <summary>
        /// This will check if the provided player type (X or O) has any diagonal line marked
        /// </summary>
        /// <param name="playerType">The player type to check diagonl marked for</param>
        /// <returns>Returns true if a the player has any diagnol line marked</returns>
        private bool PlayerMatchDiagnols(PlayerType playerType)
        {
            var diagnolStick1Match = Enumerable.Range(0, GRIDSIZE).Select(o => Cells[o][o]).Where(o => o.Player?.Type == playerType).Count() == GRIDSIZE;
            var diagnolStick2Match = Enumerable.Range(0, GRIDSIZE).Select(o => Cells[GRIDSIZE - 1 - o][o]).Where(o => o.Player?.Type == playerType).Count() == GRIDSIZE;
            return (diagnolStick1Match || diagnolStick2Match);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe.Domain
{
    public class TicTacToeGame
    {
        public GameBoard Board { get; private set; }

        public Player Player_X => Board.Player_X;

        public Player Player_O => Board.Player_O;

        public TicTacToeGame()
        {
            Board = new GameBoard();
        }

        public Player MakeMove(Player player, int x, int y)
        {
            if (player.Type == PlayerType.O && !Board.AnyCellMarked)
            {
                throw new InvalidOperationException("First move should be made by Player X");
            }
            
            if(!player.Move(x, y))
            {
                throw new InvalidOperationException("Invalid Move");
            }

            return Board.GetWinner();
        }
    }
}

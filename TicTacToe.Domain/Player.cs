﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe.Domain
{
    public class Player
    {
        public PlayerType Type { get; set; }
        public GameBoard GameBoard { get; }

        public Player(GameBoard gameBoard, PlayerType playerType)
        {
            GameBoard = gameBoard;
            Type = playerType;
        }

        public bool Move(int x, int y)
        {
            return GameBoard.GetCell(x, y).Mark(this);
        }
    }
}
